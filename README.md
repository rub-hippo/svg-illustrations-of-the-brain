# README #

This repository aims to provide a collection of high-quality SVG-figures for the neuroscientific community to be freely used in publications, presentations, websites, etc.. 

All svg-files in this repository underlie the [Create Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

Right now, there is only one file in this repository, an illustration of the hippocampus and entorhinal cortex of the rat with some axonal and dendritic morphologies. But it gives a good idea of the quality we are aiming for.